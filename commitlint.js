#!/usr/bin/env node

//-- Required Modules
const argv      = require('yargs').argv;
const shell     = require('shelljs');
const fs        = require('fs');
const path      = require('path');
const chalk     = require('chalk').default;
const logger    = require('./src/utils/log');
const config    = require('./src/utils/config');
const Validator = require('./src/utils/validator');
const Installer = require('./src/utils/installer');

//-- Ensure the user has GIT installed.
if(!shell.which('git')) {
  logger.error(`Commit lint cannot run without GIT installed!`)
}

if (argv.hasOwnProperty('install')) {
  const installer = new Installer();
  if (fs.existsSync('~/.git_template')) {
    logger.error(`commitlint already installed, '.git_template directory already registered.'`);
  }
  
  installer.install();
}

//-- Fetch the commitlint configuration from the user's PWD.
const configuration = config.loadConfigFile();
const validator     = new Validator(configuration);

//-- Validate your latest commit
const commitMessageFile = process.argv[2];
const commitMessage = fs.readFileSync(commitMessageFile, 'UTF-8');
const [subject, description = ''] = commitMessage.split('\n', 1);
validator.validate(subject, description);