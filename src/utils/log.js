const shell = require('shelljs');
const chalk = require('chalk').default;

module.exports = {

  /**
   * Logs a red error message and stops script execution.
   * 
   * @param {string} message 
   */
  error(message) {
    console.log( chalk.white.bgRed(message) );
    shell.exit(1);
  },

  /**
   * Logs a simple status message in blue.
   * 
   * @param {string} message 
   */
  log(message) {
    console.log( chalk.blue(message) );
  }

}