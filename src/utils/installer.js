const shell   = require('shelljs');
const fs      = require('fs');
const pwd     = shell.pwd().valueOf();
const path    = require('path');
const homedir = require('os').homedir();

class Installer {

  createGitTemplateDirectory() {
    shell.mkdir('-p', path.join(homedir, '.git_template/hooks'));
  }

  addGitTemplateDirectoryToGlobalConfig() {
    shell.exec(`git config --global init.templatedir '${path.join(homedir, '.git_template')}'`);
    shell.exec(`git config core.hooksPath '${path.join(homedir, '.git_template/hooks')}'`)
  }

  createCommitMsgHook() {
    const hookBody = `
    #!/bin/sh
    exec < /dev/tty
    ${path.join(homedir, '.git_template/hooks/commitlint $1')}
    `;

    fs.writeFileSync(path.join(homedir, '.git_template/hooks/commit-msg'), hookBody, 'UTF-8');
    shell.chmod('a+x', path.join(homedir, '.git_template/hooks/commit-msg'));
  }

  copyCommitlintExecutable() {
    shell.cp(path.join(pwd.valueOf(), 'commitlint'), path.join(homedir, '.git_template/hooks/'));
    shell.chmod('a+x', path.join(homedir, '.git_template/hooks/commitlint'));
  }

  install() {
    this.createGitTemplateDirectory();
    this.addGitTemplateDirectoryToGlobalConfig();
    this.createCommitMsgHook();
    this.copyCommitlintExecutable();
  }

}

const installer = new Installer();
installer.install();