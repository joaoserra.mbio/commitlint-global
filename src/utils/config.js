const fs         = require('fs');
const shell      = require('shelljs');
const pwd        = shell.pwd();
const path       = require('path');
const logger     = require('./log');
const configPath = path.join(pwd.valueOf(), 'commitlint.config.js');

module.exports = {

  /**
   * Returns a boolean value depending on whether or not the user's 
   * current working directory contains a 'commitlint.config.js' file.
   */
  configFileExists() {
    return fs.existsSync(`${configPath}`);
  },

  /**
   * Load the config file if it exists in the user's current working directory.
   */
  loadConfigFile() {
    // Early return if config file does not exist in PWD
    if( !this.configFileExists() ) {
      logger.log(`'commitlint.config.js' not found in ${pwd}`);
      shell.exit(0);
    }

    return require(configPath);
  }

}