const logger      = require('./log');
const validations = {};

class Validator {

  /**
   * Create the Validator from a configuration object.
   * 
   * @param {object} configuration 
   */
  constructor(configuration) {
    if (!configuration.hasOwnProperty('validations')) {
      logger.error(`Expected to find a 'validations' property in your configuration, but found none.`)
    }
    
    this.validations = configuration.validations;
  }

  /**
   * Validate your latest commit, by running all configuration validation 
   * methods in a loop passing in the commit subject and description.
   * 
   * @param {string} commit subject (before first newline)
   * @param {string} commit description (after first newline)
   */
  validate(subject, description = '') {
    Object.values(this.validations).forEach(validation => {
      let errorMessage = validation(subject, description);
      if (errorMessage) {
        this.renderErrorMessage(errorMessage);
        return false;
      }
    });
  }

  /**
   * Renders an error message when a validation fails to pass criteria.
   * 
   * @param {string} message 
   */
  renderErrorMessage(message) {
    logger.log(':::: COMMIT REJECTED ::::');
    logger.error( message );
  }

}

module.exports = Validator;