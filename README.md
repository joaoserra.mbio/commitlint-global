**commitlint.config.js**

```
module.exports = {
  validations: {
    maxSubjectLength: function(subject, description) {
      if (subject.length > 72) {
        return `Commit subject too long (${subject.length} characters), it should not exceed 72 characters.`
      }
    },

    maxDescriptionLength: function(subject, description) {
      if (description > 350) {
        return `Commit description too long (${description.length} characters), it should not exceed 350 characters.`
      }
    },

    subjectPattern: function(subject, description) {
      const regex = RegExp('\[CINTDS-\d{4}\]\\s.*');
      if (!regex.test(subject)) {
        return `Commit does not match the expected pattern, must begin with '[CINTDS-{issue--number}]'`
      }
    }
  }
}
```